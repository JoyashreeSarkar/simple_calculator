<?php
include'presenter.php';
class Addition{
    //defining a function
    function add2numbers($number1,$number2){
        $result=$number1+$number2;
        return $result;

    }

}
//creating a new object($addition1) from class (Addition)
$addition1=new Addition();
$result=$addition1->add2numbers($_POST['number1'],$_POST['number2']);

$displayer= new Displayer();
$displayer->displaysimple($result);
$displayer->displaypre ($result);
$displayer->displayitalic ($result);
$displayer->displayh1 ($result);